﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfo : MonoBehaviour
{
    //Varibles for everything
    public string gameName = "Kartstation";
    public int versionNumber = 0;
    public int versionSubNumber = 2;
    public string plattform = "PC";
    public string language = "EN";

    public string studio = "SP Games";

    public string getVersionString()
    {
        string r = "v" + versionNumber + "." + versionSubNumber;
        return r;
    }

    public string getNameWithVersion()
    {
        return gameName + "v" + versionNumber + "." + versionSubNumber;
    }

}
