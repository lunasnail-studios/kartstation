﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace kartstation
{

    public class MenuManager : MonoBehaviour
    {
        [Header("Animators")]
        [SerializeField] private Animator MainMenuBlock = null;
        [SerializeField] private Animator SettingsBlock = null;

        [SerializeField] private Animator GameModeBlock = null;


        public void StartGame()
        {

        }

        public void OpenSettings()
        {
            MainMenuBlock.SetTrigger("Close");
            SettingsBlock.SetTrigger("Open");
        }

        public void CloseSettings()
        {
            SettingsBlock.SetTrigger("Close");
            MainMenuBlock.SetTrigger("Open");
        }

        public void OpenGamemodes()
        {
            MainMenuBlock.SetTrigger("Close");
            GameModeBlock.SetTrigger("Open");
        }

        public void CloseGamemodes()
        {
            GameModeBlock.SetTrigger("Close");
            MainMenuBlock.SetTrigger("Open");
        }

        public void QuitGame()
        {
            Application.Quit();
            Debug.Log("Game is exiting");
        }


        public void StartMultiplayer()
        {
            SceneManager.LoadScene(1);
        }

    }
}
