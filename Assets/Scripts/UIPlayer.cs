﻿using kartstation;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace kartstation
{
    public class UIPlayer : MonoBehaviour
    {
        [SerializeField] TMP_Text text;
        Player player;

        public void SetPlayer(Player player)
        {
            this.player = player;
        }

    }
}
