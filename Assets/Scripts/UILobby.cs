﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace kartstation
{

    public class UILobby : MonoBehaviour
    {

        public static UILobby instance;


        [Header("Host Join")]
        [SerializeField] TMP_InputField joinMatchInput;
        [SerializeField] Button joinbutton;
        [SerializeField] Button hostbutton;

        [SerializeField] Canvas lobbyCanvas;

        [Header("Lobby")]
        [SerializeField] Transform UIPlayerParent;
        [SerializeField] GameObject UIPlayerPrefab;
        [SerializeField] TMP_Text matchIDText;
        [SerializeField] GameObject beginGameButton;
        [SerializeField] CustomizeManager customizeManager;

        private void Start()
        {
            instance = this;
        }


        public void Host()
        {
            joinMatchInput.interactable = false;
            joinbutton.interactable = false;
            hostbutton.interactable = false;

            Player.localPlayer.HostGame();
        }

        public void HostSuccess(bool success, string matchID)
        {
            if (success)
            {
                lobbyCanvas.enabled = true;

                SpawnPlayerUIPrefab(Player.localPlayer);
                matchIDText.text = matchID;
                beginGameButton.SetActive(true);
            }
            else
            {
                joinMatchInput.interactable = true;
                joinbutton.interactable = true;
                hostbutton.interactable = true;
            }
        }

        public void Join()
        {
            joinMatchInput.interactable = false;
            joinbutton.interactable = false;
            hostbutton.interactable = false;

            Player.localPlayer.JoinGame(joinMatchInput.text.ToUpper());
        }

        public void JoinSuccess(bool success, string matchID)
        {
            if (success)
            {
                lobbyCanvas.enabled = true;

                SpawnPlayerUIPrefab(Player.localPlayer);
                matchIDText.text = matchID;
            }
            else
            {
                joinMatchInput.interactable = true;
                joinbutton.interactable = true;
                hostbutton.interactable = true;
            }
        }

        public void SpawnPlayerUIPrefab(Player player)
        {
            GameObject newUIPlayer = Instantiate(UIPlayerPrefab, UIPlayerParent);
            newUIPlayer.GetComponent<UIPlayer>().SetPlayer(player);
            newUIPlayer.transform.SetSiblingIndex(player.playerIndex - 1);
        }

        public void BeginGame()
        {
            Player.localPlayer.BeginGame();
        }
    }
}
