﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace kartstation
{

    public class AutoHostClient : MonoBehaviour
    {
        [SerializeField] NetworkManager networkManager;

        private void Start()
        {
            if (!Application.isBatchMode)
            {
                Debug.Log($"=== CLIENT BUILD ===");
                networkManager.StartClient();
            }
            else
            {
                Debug.Log($"=== SERVER BUILD ===");
            }
        }

        public void JoinLocal()
        {
            networkManager.networkAddress = "localhost";
            networkManager.StartClient();
        }

    }
}
