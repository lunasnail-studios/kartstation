﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace kartstation
{

    public class SettingsManager : MonoBehaviour
    {
        public int Volume = 0;

        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        public void setVolumeToSlider(int b)
        {
            Volume = b;
        }

        public int getVolume()
        {
            return Volume;
        }
    }
}
