﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CustomizeManager : MonoBehaviour
{
    [Header("SkinSelector")]
    [SerializeField] private Slider artSelector = null;
    [SerializeField] private Image artImage = null;
    [SerializeField] private List<Sprite> skin = null;

    [Header("NameSelector")]
    [SerializeField] private TMP_InputField nameInput = null;

    [Header("Variables")]
    public string playerUsername;
    public int playerSkin;

    //0: red
    //1: black
    //2: yellow
    //3: green
    //4: purple

    private void Awake()
    {
        DontDestroyOnLoad(this);
        UpdateImageInSelector();
    }

    private void Start()
    {
        artSelector.maxValue = skin.Count -1;
    }

    public void UpdateSettings()
    {
        playerSkin = (int)artSelector.value;

        if(nameInput.text == "")
        {
            playerUsername = "USERNAME";
        }
        else
        {
            playerUsername = nameInput.text;
        }
    }

    public void UpdateImageInSelector()
    {
        artImage.sprite = skin[(int)artSelector.value];
    }

    public Sprite GetSprite()
    {
        return skin[playerSkin]; 
    }
}
