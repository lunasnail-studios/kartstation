﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using UnityEngine.SceneManagement;
using System;

namespace kartstation
{

    public class Player : NetworkBehaviour
    {
        public static Player localPlayer;
        [SyncVar] public string matchID;
        [SyncVar] public int playerIndex;
        [SyncVar] public int skinID;
        [SyncVar] public string playerName;

        [SerializeField] public CustomizeManager customizeManager = null;

        NetworkMatchChecker networkMatchChecker;


        void Awake()
        {
            networkMatchChecker = GetComponent<NetworkMatchChecker>();
            customizeManager = GameObject.Find("CustomizeManager").GetComponent<CustomizeManager>();
            skinID = customizeManager.playerSkin;
            playerName = customizeManager.playerUsername;
        }


        public override void OnStartClient()
        {
            if (isLocalPlayer)
            {
                localPlayer = this;
            }
            else
            {
                UILobby.instance.SpawnPlayerUIPrefab(this);
            }
        }



        /*
         *  HOST MATCH
         */

        public void HostGame()
        {
            string matchID = MatchMaker.GetRandomMatchID();
            CmdHostGame(matchID);
        }

        [Command]
        void CmdHostGame(string _matchID)
        {
            matchID = _matchID;
            if(MatchMaker.instance.HostGame(_matchID, gameObject, out playerIndex))
            {
                Debug.Log($"<color = green>Game hosted successfully</color>");
                networkMatchChecker.matchId = _matchID.ToGuid();
                TargetHostGame(true, _matchID, playerIndex);
            } else
            {
                Debug.Log($"<color = red>Game hosting failed</color>");
                TargetHostGame(false, _matchID, playerIndex);
            }
        }

        [TargetRpc]
        void TargetHostGame(bool success, string _matchID, int _playerIndex)
        {
            matchID = _matchID;
            playerIndex = _playerIndex;
            Debug.Log($"MatchID: {matchID} == {_matchID}");
            UILobby.instance.HostSuccess(success, _matchID);
        }




        /*
         *  JOIN MATCH
         */

        public void JoinGame(string _inputID)
        {
            CmdJoinGame(_inputID);
        }

        [Command]
        void CmdJoinGame(string _matchID)
        {
            matchID = _matchID;
            if (MatchMaker.instance.JoinGame(_matchID, gameObject, out playerIndex))
            {
                Debug.Log($"<color=green>Game Joined successfully</color>");
                networkMatchChecker.matchId = _matchID.ToGuid();
                TargetJoinGame(true, _matchID, playerIndex);
            }
            else
            {
                Debug.Log($"<color=red>Game Joined failed</color>");
                TargetJoinGame(false, _matchID, playerIndex);
            }
        }

        [TargetRpc]
        void TargetJoinGame(bool success, string _matchID, int _playerIndex)
        {
            playerIndex = _playerIndex;
            matchID = _matchID;
            Debug.Log($"MatchID: {matchID} == {_matchID}");
            UILobby.instance.JoinSuccess(success, _matchID);
        }



        /*
         *  BEGIN MATCH
         */

        public void BeginGame()
        {
            CmdBeginGame();
        }

        [Command]
        void CmdBeginGame()
        {
            MatchMaker.instance.BeginGame(matchID);
            Debug.Log($"<color=red>Game Beginning</color>");
        }

        public void StartGame()
        {
            TargetBeginGame();
        }

        [TargetRpc]
        void TargetBeginGame()
        {
            Debug.Log($"MatchID: {matchID} | BEGINNING");
            //Additively load game scene
            SceneManager.LoadScene(3, LoadSceneMode.Additive);
        }


        /*
         * CUSTOMIZATION
         */
        public void SetCustomization()
        {

        }
    }
}
