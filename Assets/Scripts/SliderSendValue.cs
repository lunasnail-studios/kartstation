﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace kartstation
{

    public class SliderSendValue : MonoBehaviour
    {
        [SerializeField] private SettingsManager sm = null;

        // Update is called once per frame
        void Update()
        {
            sm.setVolumeToSlider((int)this.GetComponent<Slider>().value);
        }
    }
}
